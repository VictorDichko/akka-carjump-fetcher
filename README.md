# Akka fetching service

This project is a service which fetches data from endpoint `A` at `x` second intervals
and cache results in memory


## Usage

Start services with sbt:

```
$ sbt
> ~re-start
```

With the service up, you can start sending HTTP requests:

```
$ curl http://localhost:7000/1
```
Example of successful response:
```
{
  "value": "A"
}
```
Example of request with `Not found` response:
```
$ curl http://localhost:7000/10000
```
```
{
  "code": 404,
  "message": "Incorrect index"
}
```

### Testing

Execute tests using:

```
$ sbt
> test
```
