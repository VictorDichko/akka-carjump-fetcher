import compression._
import org.scalatest._

class CompressionSpec extends FlatSpec {

  val compressor: Compressor = CompressorImpl

  val seqEmpty = Seq()
  val seqSingleUncompressed = Seq('A')
  val seqSingleCompressed = Seq(Single('A'))
  val seqMultiSingleUncompressed = Seq('A', 'B', 'C', 'D')
  val seqMultiSingleCompressed = Seq(Single('A'), Single('B'), Single('C'), Single('D'))
  val seqMultiRepeatUncompressed = Seq('A', 'A', 'A', 'A', 'A')
  val seqMultiRepeatCompressed = Seq(Repeat(5, 'A'))
  val seqMultiMixUncompressed = Seq('A', 'A', 'B', 'B', 'B', 'C', 'C', 'C', 'C')
  val seqMultiMixCompressed = Seq(Repeat(2, 'A'), Repeat(3, 'B'), Repeat(4, 'C'))
  val seqMixUncompressed = Seq('A', 'A', 'A', 'A', 'A', 'B', 'C', 'C')
  val seqMixCompressed = Seq(Repeat(5, 'A'), Single('B'), Repeat(2, 'C'))

  "Compressed" should "throw an IllegalArgumentException with negative count" in {
    intercept[IllegalArgumentException] { Compressed(-1, 'A') }
  }

  "Compressor" should "compress empty sequence into empty sequence" in {
    assert(compressor.compress(seqEmpty) === seqEmpty)
  }

  "Compressor" should "compress sequence with single value" in {
    val seqCompressed = compressor.compress(seqSingleUncompressed)
    assert(seqCompressed === seqSingleCompressed)
  }

  "Compressor" should "compress sequence with several single values" in {
    val seqCompressed = compressor.compress(seqMultiSingleUncompressed)
    assert(seqCompressed === seqMultiSingleCompressed)
  }

  "Compressor" should "compress repeated elements" in {
    val seqCompressed = compressor.compress(seqMultiRepeatUncompressed)
    assert(seqCompressed === seqMultiRepeatCompressed)
  }

  "Compressor" should "compress sequence of repeated elements" in {
    val seqCompressed = compressor.compress(seqMultiMixUncompressed)
    assert(seqCompressed === seqMultiMixCompressed)
  }

  "Compressor" should "compress sequence with both single and repeated values" in {
    val seqCompressed = compressor.compress(seqMixUncompressed)
    assert(seqCompressed === seqMixCompressed)
  }

  "Compressor" should "decompress empty sequence into empty sequence" in {
    assert(compressor.decompress(seqEmpty) === seqEmpty)
  }

  "Compressor" should "decompress sequence with single value" in {
    val seqDecompressed = compressor.decompress(seqSingleCompressed)
    assert(seqDecompressed === seqSingleUncompressed)
  }

  "Compressor" should "decompress sequence with several single values" in {
    val seqDecompressed = compressor.decompress(seqMultiSingleCompressed)
    assert(seqDecompressed === seqMultiSingleUncompressed)
  }

  "Compressor" should "decompress repeated elements" in {
    val seqDecompressed = compressor.decompress(seqMultiRepeatCompressed)
    assert(seqDecompressed === seqMultiRepeatUncompressed)
  }

  "Compressor" should "decompress sequence of repeated elements" in {
    val seqDecompressed = compressor.decompress(seqMultiMixCompressed)
    assert(seqDecompressed === seqMultiMixUncompressed)
  }

  "Compressor" should "decompress sequence with both single and repeated values" in {
    val seqDecompressed = compressor.decompress(seqMixCompressed)
    assert(seqDecompressed === seqMixUncompressed)
  }

  "Compressor" should "read from compressed empty sequence" in {
    val noElement = compressor.getFromCompressed(0, seqEmpty)
    assert(noElement === None)
  }

  "Compressor" should "read from sequence with single element" in {
    val singleElement = compressor.getFromCompressed(0, seqSingleCompressed)
    assert(singleElement.get === 'A')
  }

  "Compressor" should "read from sequence with several single values" in {
    assert(compressor.getFromCompressed(0, seqMultiSingleCompressed).get === 'A')
    assert(compressor.getFromCompressed(1, seqMultiSingleCompressed).get === 'B')
    assert(compressor.getFromCompressed(2, seqMultiSingleCompressed).get === 'C')
    assert(compressor.getFromCompressed(3, seqMultiSingleCompressed).get === 'D')
    assert(compressor.getFromCompressed(4, seqMultiSingleCompressed) === None)
  }

  "Compressor" should "read from repeated elements" in {
    assert(compressor.getFromCompressed(0, seqMultiRepeatCompressed).get === 'A')
    assert(compressor.getFromCompressed(1, seqMultiRepeatCompressed).get === 'A')
    assert(compressor.getFromCompressed(2, seqMultiRepeatCompressed).get === 'A')
    assert(compressor.getFromCompressed(3, seqMultiRepeatCompressed).get === 'A')
    assert(compressor.getFromCompressed(4, seqMultiRepeatCompressed).get === 'A')
    assert(compressor.getFromCompressed(5, seqMultiRepeatCompressed) === None)
  }

  "Compressor" should "read from sequence of repeated elements" in {
    assert(compressor.getFromCompressed(0, seqMultiMixCompressed).get === 'A')
    assert(compressor.getFromCompressed(1, seqMultiMixCompressed).get === 'A')
    assert(compressor.getFromCompressed(2, seqMultiMixCompressed).get === 'B')
    assert(compressor.getFromCompressed(3, seqMultiMixCompressed).get === 'B')
    assert(compressor.getFromCompressed(4, seqMultiMixCompressed).get === 'B')
    assert(compressor.getFromCompressed(5, seqMultiMixCompressed).get === 'C')
    assert(compressor.getFromCompressed(6, seqMultiMixCompressed).get === 'C')
    assert(compressor.getFromCompressed(7, seqMultiMixCompressed).get === 'C')
    assert(compressor.getFromCompressed(8, seqMultiMixCompressed).get === 'C')
    assert(compressor.getFromCompressed(9, seqMultiMixCompressed) === None)
  }

  "Compressor" should "read from sequence with both single and repeated values" in {
    assert(compressor.getFromCompressed(-1, seqMixCompressed) === None)
    assert(compressor.getFromCompressed(0, seqMixCompressed).get === 'A')
    assert(compressor.getFromCompressed(1, seqMixCompressed).get === 'A')
    assert(compressor.getFromCompressed(2, seqMixCompressed).get === 'A')
    assert(compressor.getFromCompressed(3, seqMixCompressed).get === 'A')
    assert(compressor.getFromCompressed(4, seqMixCompressed).get === 'A')
    assert(compressor.getFromCompressed(5, seqMixCompressed).get === 'B')
    assert(compressor.getFromCompressed(6, seqMixCompressed).get === 'C')
    assert(compressor.getFromCompressed(7, seqMixCompressed).get === 'C')
    assert(compressor.getFromCompressed(8, seqMixCompressed) === None)

  }
}
