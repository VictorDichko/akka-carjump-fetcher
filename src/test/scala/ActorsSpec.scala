import akka.actor.{Actor, ActorSystem, Props}
import akka.testkit._
import com.typesafe.config.ConfigFactory
import compression.CompressorImpl
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import scala.concurrent.duration._

import scala.concurrent.Await

class ActorsSpec
  extends TestKit(ActorSystem("TestKitUsageSpec", ConfigFactory.load("application.test.conf")))
    with WordSpecLike with Matchers with BeforeAndAfterAll with DefaultTimeout with ImplicitSender
    with ClientServiceMock {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "A Parent of ItemsGetter" should {
    "get SaveItems message in response for GetItems message" in {
      val proxy = TestProbe()
      val parent = system.actorOf(Props(new Actor {
        val child = context.actorOf(ItemsGetter.props, "testItemsGetter")

        def receive = {
          case x if sender == child => proxy.ref forward x
          case x => child forward x
        }
      }))
      proxy.send(parent, ItemsGetter.GetItems)
      proxy.expectMsgPF() {
        case DataHandler.SaveItems(_) => ()
      }
    }
  }

  "A Parent of ItemsGetter" should {
    "get SaveItems message with correct data in response for GetItems message using mock client service " in {
      val proxy = TestProbe()
      val parent = system.actorOf(Props(new Actor {
        val child = context.actorOf(Props(new ItemsGetter with ClientServiceMock), "testItemsGetter")

        def receive = {
          case x if sender == child => proxy.ref forward x
          case x => child forward x
        }
      }))
      proxy.send(parent, ItemsGetter.GetItems)
      val mockedContent = Await.result(fetchItems(), 1.second)
      val compressedItems = CompressorImpl.compress(mockedContent)
      proxy.expectMsgPF() {
        case DataHandler.SaveItems(`compressedItems`) => ()
      }
    }
  }

  "A DataHandler actor" should {
    "respond for GetItem message" in {
      val dataHandlerActor = system.actorOf(DataHandler.props, "testDataHandler")
      Thread.sleep(1000)

      val indexValid = system.settings.config.getInt("services.carjump-a.index-valid")
      val indexInvalid = system.settings.config.getInt("services.carjump-a.index-invalid")

      dataHandlerActor ! DataHandler.GetItem(indexValid)
      expectMsgPF() {
        case Some(_) => ()
      }

      dataHandlerActor ! DataHandler.GetItem(indexInvalid)
      expectMsgPF() {
        case None => ()
      }
    }
  }
}

class ActorsExtSpec
  extends TestKit(
    ActorSystem("TestKitUsageSpec",
      ConfigFactory
        .parseString("services.carjump-a.path=/B")
        .withFallback(ConfigFactory.load("application.test.conf"))))
    with WordSpecLike with Matchers with BeforeAndAfterAll with DefaultTimeout with ImplicitSender {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "A Parent of ItemsGetter" should {
    "get no message for GetItems with incorrect path parameter" in {
      val proxy = TestProbe()
      val parent = system.actorOf(Props(new Actor {
        val child = context.actorOf(ItemsGetter.props, "testItemsGetter")

        def receive = {
          case x if sender == child => proxy.ref forward x
          case x => child forward x
        }
      }))
      proxy.send(parent, ItemsGetter.GetItems)
      proxy.expectNoMsg()
    }
  }
}