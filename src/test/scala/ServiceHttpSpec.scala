import java.util.concurrent.TimeUnit

import akka.event.Logging
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.typesafe.config.ConfigFactory
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.duration._


class ServiceHttpSpec extends FlatSpec with Matchers with ScalatestRouteTest with Service {

  implicit def actorRefFactory = system

  override def testConfigSource = "akka.loglevel = WARNING"

  override def config = ConfigFactory.load("application.test.conf")

  override val logger = Logging(system, getClass)

  val host = config.getString("services.carjump-a.host")
  val path = config.getString("services.carjump-a.path")
  val timeInterval = Duration(config.getDuration("params.x", TimeUnit.SECONDS), TimeUnit.SECONDS)

  override val dataHandler = system.actorOf(DataHandler.props, "testDataHandler")

  "Service" should
    "respond to GET query" in {
    Thread.sleep(3000)

    val indexValid = config.getInt("services.carjump-a.index-valid")
    val indexInvalid = config.getInt("services.carjump-a.index-invalid")

    Get(s"/$indexValid") ~> routes ~> check {
      status shouldBe StatusCodes.OK
      contentType shouldBe `application/json`
    }

    Get(s"/$indexInvalid") ~> routes ~> check {
      status shouldBe StatusCodes.NotFound
      contentType shouldBe `application/json`
    }
  }
}