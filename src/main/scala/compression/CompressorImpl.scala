package compression

import scala.annotation.tailrec

object CompressorImpl extends Compressor {

  override def compress[A]: Seq[A] => Seq[Compressed[A]] = encode

  override def decompress[A]: Seq[Compressed[A]] => Seq[A] = decode

  override def getFromCompressed[A](index: Int, seq: Seq[Compressed[A]]): Option[A] = {
    @tailrec def find(seqUnseen: Seq[Compressed[A]], acc: Int): Option[A] = {
      if (seqUnseen.isEmpty) {
        Option.empty[A]
      } else {
        if (index < seqUnseen.head.count + acc) {
          Option(seqUnseen.head.element)
        } else {
          find(seqUnseen.tail, acc + seqUnseen.head.count)
        }
      }
    }
    if (index < 0) Option.empty[A]
    else find(seq, 0)
  }

  private def encode[T](values: Seq[T]): Seq[Compressed[T]] = {
    @tailrec def doPacking(vals: Seq[T], acc: Seq[Compressed[T]]): Seq[Compressed[T]] = {
      if (vals.nonEmpty) {
        val (first, rest) = vals.span(_ == vals.head)
        doPacking(rest, acc :+ Compressed(first.length, first.head))
      } else {
        acc
      }
    }
    doPacking(values, Seq())
  }

  private def decode[T](encoded: Seq[Compressed[T]]): Seq[T] = {
    encoded.flatMap { c => Seq.fill(c.count)(c.element) }
  }
}
