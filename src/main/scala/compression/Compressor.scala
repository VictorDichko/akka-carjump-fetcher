package compression

trait Compressor {
  def compress[A]: Seq[A] => Seq[Compressed[A]]

  def decompress[A]: Seq[Compressed[A]] => Seq[A]

  def getFromCompressed[A](index: Int, seq: Seq[Compressed[A]]): Option[A] = decompress(seq).lift(index)
}

sealed trait Compressed[+A] {
  def count: Int

  def element: A
}

case class Single[A](element: A) extends Compressed[A] {
  override val count = 1
}

case class Repeat[A](count: Int, element: A) extends Compressed[A] {
  require(count > 0)
}

object Compressed {
  def apply[A](count: Int, element: A) =
    if (count == 1) Single(element)
    else Repeat(count, element)
}
