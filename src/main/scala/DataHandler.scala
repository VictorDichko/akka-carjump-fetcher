import akka.actor.{Actor, ActorLogging, Props}
import compression.{Compressed, CompressorImpl}

import scala.concurrent.duration.Duration

object DataHandler {

  case class GetItem(index: Int)
  case class SaveItems(content: Seq[Compressed[Char]])

  def props: Props = Props(new DataHandler)
}

class DataHandler extends Actor with ActorLogging {
  import context.dispatcher
  import context.become
  import DataHandler._

  val itemsGetter = context.actorOf(ItemsGetter.props, "itemsGetter")

  val settings = Settings(context.system)

  val tick = context.system.scheduler.schedule(Duration.Zero, settings.TimeInterval, itemsGetter, ItemsGetter.GetItems)

  def cached(items: Seq[Compressed[Char]]): Receive = {
    case SaveItems(content) =>
      become(cached(content))
      log.info("Cache updated")
    case GetItem(index) =>
      sender ! CompressorImpl.getFromCompressed(index, items)
  }

  def emptyCached: Receive = {
    case SaveItems(content) =>
      become(cached(content))
    case GetItem(index) =>
      sender ! Option.empty[Char]
  }

  def receive = emptyCached

  override def postStop() = tick.cancel()
}
