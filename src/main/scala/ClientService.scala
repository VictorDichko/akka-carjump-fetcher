import java.io.IOException

import akka.actor.{Actor, ActorLogging}
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}

import scala.concurrent.Future

trait ClientService {

  def fetchItems(): Future[Array[Char]]

}

trait ClientServiceImpl extends ClientService {
  this: Actor with ActorLogging =>

  import context.dispatcher

  val settings = Settings(context.system)

  implicit val materializer = ActorMaterializer()(context.system)

  lazy val aApiConnectionFlow: Flow[HttpRequest, HttpResponse, Any] =
    Http()(context.system).outgoingConnection(settings.Host)

  def aApiRequest(request: HttpRequest): Future[HttpResponse] =
    Source.single(request).via(aApiConnectionFlow).runWith(Sink.head)

  override def fetchItems(): Future[Array[Char]] = {
    aApiRequest(RequestBuilding.Get(settings.Path)).flatMap { response =>
      response.status match {
        case StatusCodes.OK =>
          Unmarshal(response.entity).to[Array[Char]].map(_.filter(_ != '\n'))
        case _ => Unmarshal(response.entity).to[String].flatMap { entity =>
          val error = s"Request failed with status code ${response.status} and entity $entity"
          log.error(error)
          Future.failed(new IOException(error))
        }
      }
    }
  }
}

trait ClientServiceMock extends ClientService {
  def fetchItems(): Future[Array[Char]] = {
    Future.successful(Array('A', 'A', 'A', 'B', 'C', 'C'))
  }
}
