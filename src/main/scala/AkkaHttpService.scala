import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorSystem}
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}
import spray.json.{DefaultJsonProtocol, PrettyPrinter}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.language.postfixOps
import scala.util.{Failure, Success}


case class Item(value: Char)
case class Error(code: Int, message: String)

object PrettyJsonFormatSupport {
  import DefaultJsonProtocol._
  implicit val printer = PrettyPrinter
  implicit val prettyPrintedItemFormat = jsonFormat1(Item)
  implicit val prettyPrintedErrorFormat = jsonFormat2(Error)
}

trait Service {
  import PrettyJsonFormatSupport._

  implicit val system: ActorSystem

  implicit def executor: ExecutionContextExecutor

  implicit val materializer: Materializer

  def config: Config

  val logger: LoggingAdapter

  val dataHandler: ActorRef

  val routes = {
    logRequestResult("akka-carjump-fetcher") {
      get {
        pathPrefix(IntNumber) { id =>
          implicit val timeout: Timeout = 5.seconds
          val itemFuture: Future[Option[Char]] = (dataHandler ? DataHandler.GetItem(id)).mapTo[Option[Char]]

          onComplete(itemFuture) {
            case Success(item) => item match {
              case Some(value) => complete(Item(value))
              case None => complete(StatusCodes.NotFound, Error(code = 404, message = "Incorrect index"))
            }
            case Failure(ex) => complete(StatusCodes.InternalServerError, Error(code = 500, message = ex.getMessage))
          }
        }
      }
    }
  }
}

object AkkaHttpService extends App with Service {
  override implicit val system = ActorSystem()
  override implicit val executor = system.dispatcher
  override implicit val materializer = ActorMaterializer()

  override val config = ConfigFactory.load()
  override val logger = Logging(system, getClass)

  Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))

  override val dataHandler = system.actorOf(DataHandler.props, "dataHandler")
}
