import akka.actor.ActorSystem
import akka.actor.Extension
import akka.actor.ExtensionId
import akka.actor.ExtensionIdProvider
import akka.actor.ExtendedActorSystem
import scala.concurrent.duration.{FiniteDuration, Duration}
import com.typesafe.config.Config
import java.util.concurrent.TimeUnit

class SettingsImpl(config: Config) extends Extension {
  val Host: String = config.getString("services.carjump-a.host")
  val Path: String = config.getString("services.carjump-a.path")
  val TimeInterval: FiniteDuration = Duration(config.getDuration("params.x", TimeUnit.SECONDS), TimeUnit.SECONDS)
}

object Settings extends ExtensionId[SettingsImpl] with ExtensionIdProvider {
  override def lookup = Settings

  override def createExtension(system: ExtendedActorSystem) = new SettingsImpl(system.settings.config)

  override def get(system: ActorSystem): SettingsImpl = super.get(system)
}