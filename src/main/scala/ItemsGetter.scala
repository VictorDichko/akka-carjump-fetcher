import akka.actor.{Actor, ActorLogging, Props}
import compression.CompressorImpl

object ItemsGetter {

  case object GetItems

  def props: Props = Props(new ItemsGetter with ClientServiceImpl)

}

class ItemsGetter extends Actor with ActorLogging {
  this: ClientService =>

  import DataHandler._
  import ItemsGetter._
  import context.dispatcher

  def receive = {
    case GetItems =>
      fetchItems().map { charArray =>
        context.parent ! SaveItems(CompressorImpl.compress(charArray))
      }
  }
}
